package com.zuitt.s01a1;

public class DataTypesAndVariablesActivity {
    public static void main(String[] args){
        String firstName = "Roselle";
        String lastName = "Gegajo";
        float englishGrade = 87.3f;
        float mathGrade = 86.6f;
        float scienceGrade = 88.44f;

        double sum = englishGrade + mathGrade + scienceGrade;
        double average = sum / 3;

        System.out.println("Full Name: " + firstName + " " + lastName);
        System.out.println("Average Grade:" + average);

    }
}
